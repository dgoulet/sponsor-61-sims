#!/bin/bash

set -euo pipefail

# Show commands being run
set -x

mkdir sysinfo
cat /sys/devices/system/cpu/vulnerabilities/* | tee sysinfo/cpu-vulns.txt
cat /proc/cpuinfo | tee sysinfo/cpuinfo.txt
lscpu --online --parse=CPU,CORE,SOCKET,NODE | tee sysinfo/lscpu.txt
uname -a | tee sysinfo/uname.txt

# Run specified command in background and grab pid
# "$@" > /dev/null &
ulimit -c unlimited

RV=0

function finish()
{
  exitcode=`cat exitcode`
  if [ $exitcode != 0 ]; then
    echo "Simulation failed with $exitcode"
    exit exitcode
  fi

  echo "Simulation done; parsing"
  tornettools parse tornet
  tornettools plot tornet
  exit 0
}
trap finish SIGUSR1

OUTER=$$

(
  # FIXME: reenable memory manager
  tornettools \
    simulate \
    -s /root/opt/shadow/bin/shadow \
    -a "--use-cpu-pinning=true --interpose-method=preload -p $((`nproc` / 2)) --template-directory=shadow.data.template --use-memory-manager=false" \
    tornet
  echo $? > exitcode
  kill -USR1 $OUTER
) &

while true
do
  sleep 60
  echo
  date

  # processes by cpu
  # ps T --sort -pcpu -o comm,s,nlwp,pcpu,cputime,rss,wchan | head -n4
  # processes by rss
  # ps T --sort -rss -o comm,s,nlwp,pcpu,cputime,rss,wchan | head -n4

  # load averages
  # uptime
  # memory
  # free -h
  # disk
  echo df
  df -h
  echo

  #mpstat -P ALL 1 1
  echo top
  top -b1 -n1 | head -n $((`nproc` + 10))
  echo

  echo thread summary:
  ps xH -o s,comm,wchan | sed 's/[0-9]/ /g' | sort -s | uniq -c | sort -n -s || true
  echo

  # All warnings and errors (except known noisy ones)
  echo warnings:
  grep -E 'WARN|ERR' tornet/shadow.log | grep -v "we only support AF_INET" | grep -v 'Ignoring `sigaction` for SIGSYS' || echo none
  echo

  # Last log line; typically including current sim time
  echo last shadow log line:
  tail -n1 tornet/shadow.log || echo none
done

tornettools parse tornet
tornettools plot tornet
