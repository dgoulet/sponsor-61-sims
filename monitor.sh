#!/bin/bash

set -euo pipefail

# Show commands being run
set -x

TORNET=$1

mkdir $TORNET/sysinfo
cat /sys/devices/system/cpu/vulnerabilities/* | tee $TORNET/sysinfo/cpu-vulns.txt
lscpu | tee $TORNET/sysinfo/lscpu.txt
lscpu --online --parse=CPU,CORE,SOCKET,NODE | tee $TORNET/sysinfo/lscpu-parse.txt
uname -a | tee $TORNET/sysinfo/uname.txt
cat /proc/sys/kernel/pid_max | tee $TORNET/sysinfo/pid_max
cat /proc/sys/vm/max_map_count | tee $TORNET/sysinfo/max_map_count
ulimit -a | tee $TORNET/sysinfo/ulimit.txt
cat /proc/sys/kernel/threads-max | tee $TORNET/sysinfo/threads-max
cat /proc/sys/fs/file-max | tee $TORNET/sysinfo/file-max

# Run specified command in background and grab pid
# "$@" > /dev/null &
ulimit -c unlimited

while true
do
  sleep 60
  echo
  date

  # processes by cpu
  # ps T --sort -pcpu -o comm,s,nlwp,pcpu,cputime,rss,wchan | head -n4
  # processes by rss
  # ps T --sort -rss -o comm,s,nlwp,pcpu,cputime,rss,wchan | head -n4

  # load averages
  # uptime
  # memory
  # free -h
  # disk
  echo df
  df -h
  echo

  #mpstat -P ALL 1 1
  echo top
  top -b1 -n1 | head -n $((`nproc` + 10))
  echo

  echo thread summary:
  ps xH -o s,comm,wchan | sed 's/[0-9]/ /g' | sort -s | uniq -c | sort -n -s || true
  echo

  # All warnings and errors (except known noisy ones)
  #echo warnings:
  #grep -E 'WARN|ERR' $TORNET/shadow.log | grep -v "we only support AF_INET" | grep -v -E 'Ignoring `sigaction` for signal (31|11)' || echo none
  #echo

  # Last log line; typically including current sim time
  echo last shadow log line:
  tail -n1 $TORNET/shadow.log || echo none
done
