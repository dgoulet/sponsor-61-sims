#include <sys/prctl.h>
#include <stdio.h>

int main(int argc, char **argv) {
	int spec = prctl(PR_GET_SPECULATION_CTRL, PR_SPEC_STORE_BYPASS, 0, 0, 0);
	if (spec & PR_SPEC_PRCTL) {
		printf("PR_SPEC_PRCTL\n");
	}
	if (spec & PR_SPEC_ENABLE) {
		printf("PR_SPEC_ENABLE\n");
	}
	if (spec & PR_SPEC_DISABLE) {
		printf("PR_SPEC_DISABLE\n");
	} 
	if (spec & PR_SPEC_FORCE_DISABLE) {
		printf("PR_SPEC_FORCE_DISABLE\n");
	}
	return 0;
}

